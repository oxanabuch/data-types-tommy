public class RectangleAreaCalculator {

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle();
        rectangle.sideA = 4;
        rectangle.sideB = 5;
        int area = rectangle.getArea();
        System.out.println("Площа прямокутника зі сторонами " + rectangle.sideA + " та " + rectangle.sideB + ": " + area);
    }
}

class Rectangle {

    public int sideA;
    public int sideB;

    public int getArea() {
        return sideA * sideB;
    }
}