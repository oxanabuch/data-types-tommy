import java.util.Scanner;

public class DataTypes2 {

    public static void main(String[] args) {
        // одновимірний масив сума негативних елементів

        int[] numbers;
        int n;
        Scanner in = new Scanner(System.in);
        System.out.println("Enter number of numbers : ");
        n = in.nextInt();
        numbers = new int[n];

        int negativeSum = 0;
        int min = Integer.MAX_VALUE;
        int max = 1;
        int x = numbers.length;
        int indexOfMax = 0;
        int indexOfMin = 0;
        int prod = 1;
        int product = 1;
        int firstNeg = 0;
        int lastNeg;

        for (int i = 0; i < n; i++) {
            System.out.print("Enter number[" + i + "] = ");
            numbers[i] = in.nextInt();
        }

        for (int i = 0; i < numbers.length; i++) {

                if (numbers[i] < 0) {
                    negativeSum = negativeSum + numbers[i];
                }

                if (numbers[i] < min) {
                    min = numbers[i];
                    indexOfMin = i;
                }

                if (numbers[i] > max) {
                    max = numbers[i];
                    indexOfMax = i;
                }


                // одновимірний масив добуток елементів з парними числами
                if (numbers[i] % 2 == 0) {
                    product = product * numbers[i];
                }

                // одновимірний масив добуток елементів між min і max

                for (int j = (indexOfMin + 1); j != indexOfMax; j++) {
                    prod = prod * numbers[i];
                }
 /*
        // одновимірний масив сума елементів між першим і останнім негативними елементами
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] < 0) {
                firstNeg = firstNeg + 1;
                break;
            }
        }
        for (int i = numbers.length - 1; i > 0; i++) {

                    } else {
                        break;
                    }
                }
                break;
            };

        }
  */

            System.out.println();
            System.out.print("Sum of negative numbers = " + negativeSum + ", ");
            System.out.print("The minimum element = " + indexOfMin + ", ");
            System.out.print("The maximum element = " + indexOfMax + ", ");
            System.out.println("The product of elements with even numbers = " + product);
            System.out.print("Product of elements between mimimum and maximum elements = " + prod + ", ");
            System.out.println("\n");


        }
    }
}
